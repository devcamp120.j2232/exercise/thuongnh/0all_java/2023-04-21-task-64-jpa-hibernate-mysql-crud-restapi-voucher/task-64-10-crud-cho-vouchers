package com.devcamp.j64crudapi.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.j64crudapi.model.CVoucher;
import com.devcamp.j64crudapi.repository.IVoucherRepository;
import com.devcamp.j64crudapi.service.CVoucherService;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CVoucherController {
	@Autowired
	IVoucherRepository pVoucherRepository;
	@Autowired
    CVoucherService pVoucherService;
	
    //GET tất cả dữ liệu
    @GetMapping("/vouchers")
	public ResponseEntity<List<CVoucher>> getAllVouchers() {
		try {
			List<CVoucher> pVouchers = new ArrayList<CVoucher>();
			//TODO: Hãy code lấy danh sách voucher từ DB
			pVoucherRepository.findAll().forEach(pVouchers::add);
			return new ResponseEntity<>(pVouchers, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
    // Lấy danh sách voucher dùng service.
    @GetMapping("/all-vouchers")// Dùng phương thức GET
    public ResponseEntity<List<CVoucher>> getAllVouchersBySevice() {
        try {
            return new ResponseEntity<>(pVoucherService.getVoucherList(),HttpStatus.OK);            
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);   
        }
    }	
    //GET dữ liệu theo id 
    @GetMapping("/vouchers/{id}")
	public ResponseEntity<CVoucher> getCVoucherById(@PathVariable("id") long id) {
		try {
			Optional<CVoucher> voucherData = pVoucherRepository.findById(id);
			//Todo: viết code lấy voucher theo id tại đây
			if (voucherData.isPresent()) {
				return new ResponseEntity<>(voucherData.get(), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
    //Tạo MỚI voucher KHÔNG dùng service sử dụng phương thức POST
    @PostMapping("/vouchers")
	public ResponseEntity<Object> createCVoucher(@Valid @RequestBody CVoucher pVouchers) {
		try {
			//TODO: Hãy viết code tạo voucher đưa lên DB
			Optional<CVoucher> voucherData = pVoucherRepository.findById(pVouchers.getId());
			if(voucherData.isPresent()) {
				return ResponseEntity.unprocessableEntity().body(" Voucher already exsit  ");
			}
			pVouchers.setNgayTao(new Date());
			pVouchers.setNgayCapNhat(null);
			CVoucher _vouchers = pVoucherRepository.save(pVouchers);
			return new ResponseEntity<>(_vouchers, HttpStatus.CREATED);			
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			//Hiện thông báo lỗi tra back-end
			//return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			//return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified Voucher: "+e.getCause().getCause().getMessage());
		}
	}
    // Sửa/update voucher theo {id} KHÔNG dùng service, sử dụng phương thức PUT
    @PutMapping("/vouchers/{id}")// Dùng phương thức PUT
	public ResponseEntity<Object> updateCVoucherById(@PathVariable("id") long id, @RequestBody CVoucher pVouchers) {		
		try {	
			//TODO: Hãy viết code lấy voucher từ DB để UPDATE		
			Optional<CVoucher> voucherData = pVoucherRepository.findById(id);
			if (voucherData.isPresent()) {
				CVoucher voucher= voucherData.get();
				voucher.setMaVoucher(pVouchers.getMaVoucher());
				voucher.setPhanTramGiamGia(pVouchers.getPhanTramGiamGia());
				voucher.setNgayCapNhat(new Date());

				return new ResponseEntity<>(pVoucherRepository.save(voucher), HttpStatus.OK);					
			} else {
				return ResponseEntity.badRequest().body("Failed to get specified Voucher: "+id + "  for update.");
			}			
		} catch (Exception e) {
			System.out.println(e);
			//return ResponseEntity.unprocessableEntity().body("Failed to Update specified Voucher:"+e.getCause().getCause().getMessage());
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
    // Xoá/delete voucher theo {id} KHÔNG dùng service, sử dụng phương thức DELETE
    @DeleteMapping("/vouchers/{id}")// Dùng phương thức DELETE
	public ResponseEntity<CVoucher> deleteCVoucherById(@PathVariable("id") long id) {
		try {
			pVoucherRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
    // Xoá/delete tất cả voucher KHÔNG dùng service, sử dụng phương thức DELETE
    @DeleteMapping("/vouchers")// Dùng phương thức DELETE
	public ResponseEntity<CVoucher> deleteAllCVoucher() {
		try {
			pVoucherRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
